import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  TextInput,
  StatusBar,
} from 'react-native';

import Grid from 'react-native-grid-component';

import {
  Card,
  CardActions,
  CardContent,
  CardCover,
  Title,
  Paragraph
} from 'react-native-paper';

//import { material } from 'react-native-typography';

// import AnimatedNumber from "animated-number-vue";

//  import {fetchData} from './api/api';

export default class Cards extends React.Component{

  constructor(props){
    super(props);
    //console.log(props);
    
  }
  render(){
    // if(!confirmed){
    //   return 'loading....';
    // }
    return(
      <ScrollView  horizontal={true} style = {{backgroundColor:"orange"}} >
      <View style = {styles.container}>
    <Card style = {{backgroundColor:'yellow'}}>
          <Card.Title title="INFECTED" subtitle="Confirmed cases"/>
    <Card.Content>
          <Title>Real Data</Title>
          <Title>Real Date</Title>
          <Paragraph style = {styles.b}>Number of active cases of COVID-19</Paragraph>
    </Card.Content>
    </Card>
    <Card style = {{backgroundColor:'yellow'}}>
          <Card.Title title="RECOVERED" subtitle="Back safe at home"/>
    <Card.Content>
         <Title>Real Data</Title>
          <Title>Real Date</Title>
          <Paragraph style = {styles.g}>Number of active cases of COVID-19</Paragraph>
    </Card.Content>
    </Card>
    <Card style = {{backgroundColor:'yellow'}}>
          <Card.Title title="DEATHS" subtitle="Fatal cases"/>
    <Card.Content>
          <Title>Real Data</Title>
          <Title>Real Date</Title>
          <Paragraph style = {styles.r}>Number of active cases of COVID-19</Paragraph>
    </Card.Content>
    </Card>
    </View>
    </ScrollView>
    );
  }
}
const styles = StyleSheet.create(
  {
        container:{
          flex:1, 
          backgroundColor:'yellow',
          alignItems:'center',
          justifyContent:'space-around',
          flexDirection:'row',
        },
        text:{
          color:'green',
          fontSize:15,
        },
        list: {
           flex: 1,
           justifyContent:'center',
           alignItems:'center',
        },
        g:{
          color:'green',
        },
        r:{
          color:'red',
        },
        b:{
          color:'blue',
        },
  }
)