import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  TextInput,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

// import AnimatedNumber from "animated-number-vue";

import Cards from './components/cards/cards';               //importing components files to app.js
import Charts from './components/charts/charts';
import CountryPicker from './components/countryPicker/countryPicker';
//import {Cards,Charts,CountryPicker} from './components';

import {fetchData} from './api/api';

export default class App extends React.Component{
  
  state ={
    data:{},
  }

  async componentDidMount(){
    const data = await fetchData();
    //console.log(data);
    this.setState(
     {data:fetchData}
    );
  }

  render(){
    return(
      <View style ={styles.container}>                        
        <Text style ={styles.text}>CORONA TRACKER</Text>

        <Cards data={this.state.data}/>         

        <CountryPicker/>
        
        <Charts/>   
        
      </View>
    );
  }
}

const styles = StyleSheet.create(
  {
        container:{
          backgroundColor:'lightgreen',
          flex:1, 
          alignItems:'center',
          justifyContent:'space-around',
        },
        text:{
          fontSize:30,
        },
  }
)