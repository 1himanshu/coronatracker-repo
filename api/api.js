import axios from 'axios';

const url = "https://covid19.mathdro.id/api";

export const fetchData = async () => {                              //fetching data using axios get request
    try {                                                            //we will be using try & catch
    
        const {data:{confirmed,recovered,deaths,lastupdate}} = await axios.get(url);
        
        // const modifiedData = {                                //creating object or
        //     confirmed,      //: confirmed,
        //     recovered,      //: recovered,
        //     deaths,         //: deaths,
        //     lastupdate,     //: lastupdate, 
        // }
       
        return {                                              //rather than storing directly returning it
            confirmed,      //: confirmed,
            recovered,      //: recovered,
            deaths,         //: deaths,
            lastupdate    //: lastupdate, 
        };
    } catch (error) {
        
    }
}